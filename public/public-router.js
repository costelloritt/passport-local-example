'use strict';

const router = require('express').Router();

// URL: localhost:3000/

const publicAuthRouter = require('./public-auth/router');

router.use('/web-auth', publicAuthRouter);

module.exports = router;
