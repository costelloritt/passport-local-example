'use strict';

const router = require('express').Router();
const passport = require('passport');

const indexController = require('./controllers/index-controller');

// URL: localhost:3000/web-auth

router.get('/signup', (req, res) => {
  return res.status(200).json({
    message: 'This is the sign up page. Please sign up.'
  });
});

router.get('/login-failure', (req, res) => {
  return res.status(200).json({
    message: 'Invalid username or password. Please try logging in again or create a new account at /web-auth/signup'
  });
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/web/signup-success',
  failureRedirect: '/web-auth/signup'
}), indexController.signupError);

router.post('/login', passport.authenticate('local-login', {
  successRedirect: '/web/login-success',
  failureRedirect: '/web-auth/login-failure'
}), indexController.loginError);

module.exports = router;
