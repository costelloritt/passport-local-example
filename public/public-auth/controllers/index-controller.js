'use strict';

exports.signupError = (err, req, res, next) => {
  console.log(err);
  return res.status(500).json({
    error: 'It looks like we had a problem signing you up. Please try again.'
  });
};

exports.loginError = (err, req, res, next) => {
  console.log(err);
  return res.status(500).json({
    error: 'It looks like we had a problem logging you in. Please try again.'
  });
};
