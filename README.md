# Authentication with Passport Local Strategy
This project is an example of authentication management using the local strategy (username and password) with Passport. The project also has light weight authorization in place.

### Run the Project
1. Use the SQL script in SQL/user.sql to create the 'user' table. This project is only compatible with MySQL.
2. Change the name of the exampleEnv.txt file to ".env" and replace the values with your own.
3. Run ```npm install --save``` inside the project root.
4. Run ```npm start``` to run the project.
5. Use the list of endpoints below to explore the application.

### List of Endpoints

#### No Authentication or Authorization Required

GET /web-auth/signup

GET /web-auth/login-failure

GET /logout

POST /web-auth/signup

    DATA: {
            "username": "your_username",
            "password": "your_password"
          }

POST /web-auth/login

    DATA: {
            "username": "your_username",
            "password": "your_password"
          }

#### Authentication and Authorization Required

GET /web/free-module/status  
Authorization: is_free

GET /web/member-module/status  
Authorization: is_member

GET /web/user-settings/change-password  
Authorization: is_free

POST /web/user-settings/change-password  
Authorization: is_free

    DATA: {
            "currentPassword": "yourPassword",
            "newPassword": "yourNewPassword",
            "newPasswordConfirm": "yourNewPassword"
          }

GET /web/manage-users/alter-user-permission  
Authorization: is_admin

POST /web/manage-users/alter-user-permission  
Authorization: is_admin

    DATA: {
            "userId": "usersId",
            "username": "usersUsername",
            "accessLevels": {
                "isLocked": 1 or 0,
                "isFree": 1 or 0,
                "isMember": 1 or 0,
                "isAdmin": 1 or 0
            }
          }
