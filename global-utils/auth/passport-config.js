'use strict';

const path = require('path');
const localStrategy = require('passport-local').Strategy;
const db = require(path.resolve(__dirname, '../database/dbconfig'));
const bcryptUtils = require('./bcrypt-utils');

module.exports = function (passport) {
  // serialize information into session
  passport.serializeUser(function(user, done) {
    return done(null, user.id);
  });

  // deserialize information from session
  passport.deserializeUser(function(id, done) {
    db.query("SELECT id, username, is_locked, is_free, is_member, is_admin FROM user WHERE id = ?", [id], (err, results) => {
      if (err) {
        return done(err);
      }
      const accessLevels = {
        is_locked: results[0].is_locked,
        is_free: results[0].is_free,
        is_member: results[0].is_member,
        is_admin: results[0].is_admin
      };
      const desUser = {
        id: results[0].id,
        username: results[0].username,
        access: accessLevels
      };
      return done(null, desUser);
    });
  });

  // local signup
  passport.use('local-signup', new localStrategy(
    function(username, password, verified) {
      // check if username exists and create new user if user does not exist
      return selectUserFromDB(username, password, addNewUserToDB, verified);
    })
  );

  // local login
  passport.use('local-login', new localStrategy(
    function(username, password, verified) {
      // log in existing user functionality
      return authenticateUsernameAndPassword(username, password, verified);
    })
  );
};

function selectUserFromDB (username, password, callback, verified) {
  db.query('SELECT username FROM user WHERE username = ?', [username], (err, results) => {
    if (err) {
      return verified(err);
    }
    if (results.length > 0) {
      return verified(null, false, 'username unavailable');
    } else {
      // The callback is the function: addNewUserToDB(username, password);
      return callback(username, password, verified);
    }
  });
}

function addNewUserToDB (username, password, verified) {
  bcryptUtils.hashPassword(password, (err, hash) => {
    if (err) {
      return verified(err);
    }
    db.query('INSERT INTO user (username, password) VALUES (?,?)', [username, hash], (err, results) => {
      if (err) {
        return verified(err);
      }
      var newUser = {
        id: results.insertId,
        username: username
      };
      return verified(null, newUser);
    });
  });
}

function authenticateUsernameAndPassword (username, password, verified) {
  db.query('SELECT id, username, password FROM user WHERE username = ?', [username], (err, results) => {
    if (err) {
      return verified(err);
    }
    bcryptUtils.compareSuppliedPasswordWithExisting(password, results[0].password, (err, res) => {
      if (err) {
        return verified('ERROR');
      }
      if (res === true) {
        return verified(null, {id: results[0].id, username: results[0].username});
      } else {
        return verified(null, false, 'authentication unsuccessful');
      }
    });
  });
}
