'use strict';

const authorizationConfig = require('./authorization-config');

exports.authorize = (req, res, next) => {
  // get base URL path
  var apiUrl = req.baseUrl;
  // remove first slash
  apiUrl = apiUrl.substring(1);
  const parsedUrl = apiUrl.split('/');
  // get the last value. This should be the module name, such as "user-settings"
  const apiModule = parsedUrl[parsedUrl.length - 1];
  // retrieve the current user's access levels that were retrieved during
  // deserialization in passport-config.js
  const userAccessLevels = req.user.access;
  if (userAccessLevels.is_locked !== 0) {
    return res.status(403).json({
      error: 'Your account is locked.'
    });
  } else if (userAccessLevels.is_member !== 1 && userAccessLevels.is_free === 1 && authorizationConfig.is_free.indexOf(apiModule) !== -1) {
    // free user only. Has access to the module found in the URL
      return next();
  } else if (userAccessLevels.is_member === 1 && userAccessLevels.is_free === 1 && authorizationConfig.is_member.indexOf(apiModule) !== -1) {
    // paid user "member." Has access to the module found in the URL
    return next();
  } else if (userAccessLevels.is_admin === 1 && authorizationConfig.is_admin.indexOf(apiModule) !== -1) {
    // administrator has access to both free and paid modules and administrative modules
    return next();
  } else {
    // if the user doesn't have access to the module they are requesting.
    return res.status(403).json({
      error: 'You do not have the proper authorization to view this page.'
    });
  }
};
