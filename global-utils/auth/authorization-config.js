'use strict';

/**
 * The Authorization config file is where the authorization levels for the 3 different
 * access levels are defined. The AuthZ service, authorization-service.js, gets the
 * information from this file to determine whether or not users have authorization
 * to access the resources they are requesting.
 *
 * ACCESS LEVELS:
 *
 * 1. is_locked - the user account is locked. It will have no or almost no access
 *                to functionality. This will be unimplemented for now, other than
 *                a check to see if an account is locked. If it is locked, it will
 *                not be able to access anything.
 *
 * 2. is_free - the user account is a free account. By default, any new user account
 *              is classified as a free account. The permission level is used to
 *              specify any modules that accounts that do not pay for services have
 *              access to.
 *
 * 3. is_member - the user account is a paid account. This permission level will be
 *                enabled when a user account pays for member functionality. No
 *                implementations of this funcitonality are yet planned.
 *
 * 4. is_admin - the user account is an administrative account. This should only be
 *               active for users who help manage the application.
 */

const config = {
  is_free: [
    'free-module',
    'user-settings'
  ],
  is_member: [
    'free-module',
    'member-module',
    'user-settings'
  ],
  is_admin: [
    'free-module',
    'member-module',
    'manage-users',
    'user-settings'
  ]
};

module.exports = config;
