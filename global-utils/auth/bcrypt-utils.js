'use strict';

const bcrypt = require('bcrypt');

exports.hashPassword = (password, callback) => {
  const saltRounds = 10;
  bcrypt.hash(password, saltRounds, (err, hash) => {
    // store hash in db
    return callback(err, hash);
  });
}

exports.compareSuppliedPasswordWithExisting = (suppliedPassword, existingPassword, callback) => {
  bcrypt.compare(suppliedPassword, existingPassword, callback);
};
