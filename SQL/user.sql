CREATE SCHEMA passport_local_example;

USE passport_local_example;

CREATE TABLE `user` (
	`id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_locked`tinyint NOT NULL DEFAULT 0,
  `is_free` tinyint NOT NULL DEFAULT 1,
  `is_member`tinyint NOT NULL DEFAULT 0,
  `is_admin`tinyint NOT NULL DEFAULT 0,
  `created_at`TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;
