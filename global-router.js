'use strict';

const router = require('express').Router();

// URL: localhost:3000

const publicRouter = require('./public/public-router');
const privateRouter = require('./private/private-router');

router.use('/web', isLoggedIn, privateRouter);
router.use('/', publicRouter);

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/web-auth/signup');
}
