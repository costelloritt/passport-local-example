'use strict';

// load current .env file if not production
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load();
}

// npm
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const session = require('express-session');
const passport = require('passport');

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

// init session
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {maxAge: 7200000},
    name: 'rcp',
    unset: 'destroy'
  })
);

require('./global-utils/auth/passport-config')(passport);
app.use(passport.initialize());
app.use(passport.session());

const globalRouter = require('./global-router');

app.use('/', globalRouter);

app.get('/logout', (req, res) => {
  req.logout();
  req.session = null;
  res.status(200).json({
    message: 'You are now logged out.'
  });
});

app.use((req, res) => {
  res.status(404).json({
    "error": "404 not found."
  });
});

app.listen(process.env.PORT, () => {
  console.log(`listening on port: ${process.env.PORT}`)
});
