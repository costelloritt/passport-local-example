'use strict';

const router = require('express').Router();
const authzService = require('../global-utils/auth/authorization-service');

const userSettingsRouter = require('./user-settings/router');
const freeModuleRouter = require('./free-module/router');
const memberModuleRouter = require('./member-module/router');
const manageUsersRouter = require('./user-management/router');

router.get('/signup-success', (req, res) => {
  return res.status(200).json({
    message: 'authentication through signup successful.'
  });
});

router.get('/login-success', (req, res) => {
  return res.status(200).json({
    message: 'authentication through login successful.'
  });
});

router.use('/user-settings', authzService.authorize, userSettingsRouter);
router.use('/free-module', authzService.authorize, freeModuleRouter);
router.use('/member-module', authzService.authorize, memberModuleRouter);
router.use('/manage-users', authzService.authorize, manageUsersRouter);

module.exports = router;
