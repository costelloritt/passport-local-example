'use strict';

const userAuthService = require('../services/user-auth-service');

exports.getChangePassword = (req, res) => {
  res.status(200).json({
    message: `Please send a POST request to this same URL with the following POST data to reset your password:
    {
      "currentPassword": "yourPassword",
      "newPassword": "yourNewPassword",
      "newPasswordConfirm": "yourNewPassword"
    }`
  });
};

exports.changePassword = (req, res) => {
  const currentPassword = req.body.currentPassword;
  const newPassword = req.body.newPassword;
  const newPasswordConfirm = req.body.newPasswordConfirm;

  if (newPassword !== newPasswordConfirm) {
    return res.status(200).json({
      message: 'It looks like you supplied an incorrect password or your new passwords do not match. Please try again.'
    });
  }

  userAuthService.verifyUserReAuthenticationAndChangePassword(req, currentPassword, newPassword, (results) => {
    if (results.error === true) {
      return res.status(500).json({
        error: 'It looks like we ran into an error. Please try again.'
      });
    }

    if (results.isReAuthenticated === false) {
      return res.status(200).json({
        message: 'It looks like you supplied an incorrect password or your new passwords do not match. Please try again.'
      });
    }

    if (results.isReAuthenticated === true && results.isPasswordUpdated === true) {
      return res.status(200).json({
        message: 'Your password was changed successfully.'
      });
    }
  });
};
