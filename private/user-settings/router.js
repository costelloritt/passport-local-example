'use strict';

const router = require('express').Router();
const userSettingsController = require('./controllers/user-settings-controller');

router.get('/change-password', userSettingsController.getChangePassword);
router.post('/change-password', userSettingsController.changePassword);

module.exports = router;
