'use strict';

const db = require('../../../global-utils/database/dbconfig');
const bcryptUtils = require('../../../global-utils/auth/bcrypt-utils');

exports.verifyUserReAuthenticationAndChangePassword = (req, currentPassword, newPassword, callback) => {
  db.query('SELECT id, username, password FROM user WHERE id = ?', [req.user.id], (err, results) => {
    if (err) {
      return callback({
        error: true,
        isReAuthenticated: false,
        isPasswordUpdated: false
      });
    }
    bcryptUtils.compareSuppliedPasswordWithExisting(currentPassword, results[0].password, (err, res) => {
      if (err) {
        return callback({
          error: true,
          isReAuthenticated: false,
          isPasswordUpdated: false
        });
      }
      if (res === true) {
        // update user password
        bcryptUtils.hashPassword(newPassword, (err, hash) => {
          if (err) {
            return callback({
              error: true,
                isReAuthenticated: true,
                isPasswordUpdated: false
            });
          }
          db.query('UPDATE user SET password = ? WHERE id = ?', [hash, req.user.id], (err, results) => {
            if (err) {
              return callback({
                error: true,
                isReAuthenticated: true,
                isPasswordUpdated: false
              });
            } else {
              return callback({
                error: false,
                isReAuthenticated: true,
                isPasswordUpdated: true
              });
            }
          });
        });
      } else {
        return callback({
          error: false,
          isReAuthenticated: false,
          isPasswordUpdated: false
        });
      }
    });
  });
};
