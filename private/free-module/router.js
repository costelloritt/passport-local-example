'use strict';

const router = require('express').Router();
const freeModuleController = require('./controllers/free-module-controller');

router.get('/status', freeModuleController.getStatus);

module.exports = router;
