'use strict';

const db = require('../../../global-utils/database/dbconfig');

exports.updateUserPermissions = (userId, username, accessLevels, callback) => {
  db.query(`UPDATE user SET is_locked = ?, is_free = ?,
    is_member = ?, is_admin = ? WHERE id = ? AND username = ?`,
    [accessLevels.isLocked, accessLevels.isFree,
    accessLevels.isMember, accessLevels.isAdmin,
    userId, username],
    (err, results) => {
      if (err) {
        return callback({
          error: true,
          isPermissionUpdated: false
        });
      } else if (results.affectedRows === 0) {
        return callback({
          error: false,
          isPermissionUpdated: false
        });
      } else if (results.affectedRows === 1) {
        return callback({
          error: false,
          isPermissionUpdated: true
        });
      }
    }
  );
};
