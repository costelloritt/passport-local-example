'use strict';

const router = require('express').Router();
const manageUsersController = require('./controllers/manage-users-controller');

router.get('/alter-user-permission', manageUsersController.getAlterUserPermission);
router.post('/alter-user-permission', manageUsersController.alterUserPermission);

module.exports = router;
