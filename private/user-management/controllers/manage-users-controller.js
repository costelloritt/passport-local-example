'use strict';

const manageUsersService = require('../services/manage-users-service');

exports.getAlterUserPermission = (req, res) => {
  return res.status(200).json({
    message: `As an administrator, you can alter user permissions. Send a request to this
    same URL with the following POST data to alter user permissions:
    {
      "userId": "usersId",
      "username": "usersUsername",
      "accessLevels": {
        "isLocked": 1 or 0,
        "isFree": 1 or 0,
        "isMember": 1 or 0,
        "isAdmin": 1 or 0
      }
    }`
  });
};

exports.alterUserPermission = (req, res) => {
  const userId = req.body.userId;
  const username = req.body.username;
  const accessLevels = req.body.accessLevels;

  manageUsersService.updateUserPermissions(userId, username, accessLevels, (results) => {
    if (results.error === true) {
      return res.status(500).json({
        error: 'It looks like we ran into an error. Please try again.'
      });
    }

    if (results.error === false && results.isPermissionUpdated == true) {
      return res.status(200).json({
        message: `User permissions for ${username} have been successfully updated.`
      });
    } else {
      return res.status(200).json({
        message: `Error updating user permissions for ${username}.`
      });
    }
  });
};
