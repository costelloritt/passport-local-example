'use strict';

exports.getStatus = (req, res) => {
  res.status(200).json({
    message: 'You have reached the member module. Status: OK'
  });
};
