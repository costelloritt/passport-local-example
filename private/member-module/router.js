'use strict';

const router = require('express').Router();
const memberModuleController = require('./controllers/member-module-controller');

router.get('/status', memberModuleController.getStatus);

module.exports = router;
